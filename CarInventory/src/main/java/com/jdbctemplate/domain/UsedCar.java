package com.jdbctemplate.domain;

import java.io.Serializable;

public class UsedCar implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String make;
private String model;
private int year;
private float salesPrice;
public String getMake() {
return make;
}
public void setMake(String make) {
this.make = make;
}
public String getModel() {
return model;
}
public void setModel(String model) {
this.model = model;
}
public int getYear() {
return year;
}
public void setYear(int year) {
this.year = year;
}
public float getSalesPrice() {
return salesPrice;
}
public void setSalesPrice(float salesPrice) {
this.salesPrice = salesPrice;
}

}
