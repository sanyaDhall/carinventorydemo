package com.jdbctemplate.service;

import java.util.List;

import com.jdbctemplate.domain.UsedCar;

public interface UsedCarDao {
public void saveCars(UsedCar usedCar);
    public List<UsedCar> getAllCars();
   

}