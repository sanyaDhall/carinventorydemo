package com.jdbctemplate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.jdbctemplate.domain.UsedCar;

public class UsedCarDaoImpl implements UsedCarDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void saveCars(UsedCar usedCar) {
	 String sql = "insert into usedCar values(?,?,?,?)";
	       jdbcTemplate.update(sql, new Object[] {usedCar.getMake(),usedCar.getModel(),usedCar.getYear(),usedCar.getSalesPrice()});
	}


	public List<UsedCar> getAllCars() {
	return jdbcTemplate.query("SELECT * FROM usedCar",
	                BeanPropertyRowMapper.newInstance(UsedCar.class));
	}
}
