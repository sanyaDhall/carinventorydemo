package com.jdbctemplate.main;

import java.util.List;
import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jdbctemplate.domain.UsedCar;
import com.jdbctemplate.service.UsedCarDaoImpl;

public class ClassInventoryMain {
 static UsedCarDaoImpl dao;  
 @SuppressWarnings("unchecked")
public static void main(String[] args)
   {
 ApplicationContext context=new ClassPathXmlApplicationContext("Beans.xml");  
     dao = (UsedCarDaoImpl) context.getBean("UsedCarDaoImpl");

 System.out.println("Welcome to Mullet Joe's Gently Used Autos!");
 Scanner in = new Scanner(System.in);
 String task = in.nextLine();
     System.out.println("Enter command: " + task);
   
     switch (task) {
       case "add":
        String Make=in.nextLine();
           String Model=in.nextLine();
           int Year=in.nextInt();
           float SalesPrice=in.nextFloat();
           System.out.println("Make : "+Make);
           System.out.println("Model : "+Model);
           System.out.println("Year : "+Year);
           System.out.println("SalesPrice ($): "+SalesPrice);
           
           UsedCar usedCar = new UsedCar();
                 usedCar.setMake(Make);
              usedCar.setModel(Model);
            usedCar.setYear(Year);
            usedCar.setSalesPrice(SalesPrice);
         
       //saving of UsedCar
       dao.saveCars(usedCar);
           
           break;
       case "quit":
           System.out.println("  Good bye! ");
           break;
       case "list":
           List<UsedCar> usedCarsList = dao.getAllCars();
           if(usedCarsList.isEmpty())
           {
            System.out.println("There are currently no cars in the catalog.");
           }
           
           else        
          displayListData();
           break;
       default:
           System.out.println("Sorry, but "+task+" is not a valid command. Please try again");
           break;
     
     }
   }

@SuppressWarnings("unchecked")
private static void displayListData() {
List<UsedCar> usedCarsList = dao.getAllCars();
         
      usedCarsList.forEach(car ->System.out.println(car.getYear()+" "+car.getMake()+" "+car.getModel()+" $"+car.getSalesPrice()));
      Float totalPrice = usedCarsList.stream()  
                .map(usedCar->usedCar.getSalesPrice())  
                .reduce(0.0f,(sum, price)->sum+price);  
       
        System.out.println("");
        System.out.println("Number of Cars : "+ usedCarsList.size());
        System.out.println("Total Inventory : $"+totalPrice);
       
}
}
